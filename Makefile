.PHONY: all re clean fclean

SRC = answer.c main.c

SRO = $(SRC:.c=.o)

NAME = akorchyn.filler

LIBFT = libft/libft.a

all: $(NAME)

$(NAME):
	@make re -C libft
	@gcc -c -Wall -Wextra -Werror $(SRC)
	@gcc -Wall -Wextra -Werror $(LIBFT) $(SRO) -o $(NAME)

clean:
	@make clean -C libft
	@/bin/rm -rf $(SRO)

fclean: clean
	@/bin/rm -rf $(LIBFT) $(NAME)

re: fclean all
