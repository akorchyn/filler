/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 15:54:48 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/16 14:40:56 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AKORCHYN_FILLER_FILLER_H
# define AKORCHYN_FILLER_FILLER_H

# include "libft/includes/libft.h"

# define ABS(x)(((x) < 0) ? (-(x)) : (x))

char					g_player;
char					g_enemy;

typedef struct			s_cordinates
{
	int					x;
	int					y;
	int					manhetten;
	struct s_cordinates	*next;
}						t_cordinates;

typedef struct			s_matrix
{
	int					x;
	int					y;
	char				**map;
}						t_matrix;

void					set(t_cordinates *enemy, t_matrix map, t_matrix figure);
t_cordinates			*fill_list(t_matrix map);
void					free_lists(t_cordinates *list);

#endif
