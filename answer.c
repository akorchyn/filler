/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   answer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 14:36:23 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/18 17:02:27 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int				check(t_matrix map, t_matrix figure, int x, int y)
{
	int		s_x;
	int		s_y;
	int		times;

	s_y = -1;
	times = 0;
	while (++s_y < figure.y)
	{
		s_x = -1;
		while (++s_x < figure.x)
			if (figure.map[s_y][s_x] == '.')
				;
			else if (s_x + x > -1 && s_x + x < map.x
			&& s_y + y > -1 && s_y + y < map.y
			&& (map.map[s_y + y][s_x + x] == '.'
			|| map.map[s_y + y][s_x + x] == g_player
			|| map.map[s_y + y][s_x + x] == g_player - 32))
				(map.map[s_y + y][s_x + x] == g_player ||
				map.map[s_y + y][s_x + x] == g_player - 32) ? times++ : 0;
			else if (times > 1)
				return (0);
			else
				return (0);
	}
	return (times == 1);
}

t_cordinates			*fill_list(t_matrix map)
{
	int				x;
	int				y;
	t_cordinates	*head;
	t_cordinates	*tmp;

	head = NULL;
	y = -1;
	while (++y < map.y)
	{
		x = -1;
		while (++x < map.x)
		{
			if (map.map[y][x] == g_enemy || map.map[y][x] == g_enemy - 32)
			{
				tmp = (t_cordinates *)malloc(sizeof(t_cordinates));
				tmp->next = head;
				tmp->x = x;
				tmp->y = y;
				head = tmp;
			}
		}
	}
	return (head);
}

static t_cordinates		*new_points_l(int x, int y, t_matrix figure)
{
	int				s_x;
	int				s_y;
	t_cordinates	*tmp;
	t_cordinates	*head;

	s_y = -1;
	head = NULL;
	while (++s_y < figure.y)
	{
		s_x = -1;
		while (++s_x < figure.x)
		{
			if (figure.map[s_y][s_x] == '*')
			{
				tmp = (t_cordinates *)malloc(sizeof(t_cordinates));
				tmp->next = head;
				tmp->x = x + s_x;
				tmp->y = s_y + y;
				head = tmp;
			}
		}
	}
	return (head);
}

static int				manh(t_cordinates *points, t_cordinates *enemy)
{
	t_cordinates	*run;
	int				min;
	int				res;

	min = -1;
	while (points)
	{
		run = enemy;
		while (run)
		{
			res = ABS(points->x - run->x) + ABS(points->y - run->y);
			if (res < min || min == -1)
				min = res;
			run = run->next;
		}
		points = points->next;
	}
	free_lists(points);
	return (min);
}

void					set(t_cordinates *enemy, t_matrix map, t_matrix figure)
{
	t_cordinates		place;
	t_cordinates		i;

	i.y = -1 - figure.y;
	place.manhetten = -1;
	while (++i.y < map.y + figure.y)
	{
		i.x = -1 - figure.x;
		while (++i.x < map.x + figure.x)
			if (check(map, figure, i.x, i.y))
			{
				i.manhetten = manh(new_points_l(i.x, i.y, figure), enemy);
				if (i.manhetten < place.manhetten || place.manhetten == -1
					|| (i.manhetten == place.manhetten && map.x > 20))
				{
					place.manhetten = i.manhetten;
					place.x = i.x;
					place.y = i.y;
				}
			}
	}
	(place.manhetten == -1) ? ft_printf("0 0\n")
							: ft_printf("%d %d\n", place.y, place.x);
}
