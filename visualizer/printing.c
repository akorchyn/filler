/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 19:52:27 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/19 16:41:16 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "for_visual.h"

void			change_colors(t_matrix map, t_cordinates a, int x, int y)
{
	while (++y < map.y && (x = -1))
		while (++x < map.x)
			if (map.map[y][x] == 'o')
				mvaddch(y + a.y, x + a.x, ' ' | COLOR_PAIR(LAST_SET_1_PLAYER));
			else if (map.map[y][x] == 'x')
				mvaddch(y + a.y, x + a.x, ' ' | COLOR_PAIR(LAST_SET_2_PLAYER));
			else if (map.map[y][x] == 'O')
				mvaddch(y + a.y, x + a.x, ' ' | COLOR_PAIR(BACKGROUND_1));
			else if (map.map[y][x] == 'X')
				mvaddch(y + a.y, x + a.x, ' ' | COLOR_PAIR(BACKGROUND_2));
			else
				mvaddch(y + a.y, x + a.x, ' ' | COLOR_PAIR(DEFAULT_WINDOW));
}

void			print_names(char *line, t_cordinates window)
{
	char		*tmp;
	int			isfirst;

	isfirst = (line[10] == '1') ? 1 : 0;
	tmp = ft_strrchr(line, '/');
	tmp++;
	attron(A_BOLD);
	if (isfirst)
	{
		attron(COLOR_PAIR(TEXT_1_PLAYER));
		mvaddstr(2, 2, "p1: ");
		mvaddnstr(2, 6, tmp, ft_strlen(tmp) - 1);
		attroff(COLOR_PAIR(TEXT_1_PLAYER));
	}
	else
	{
		attron(COLOR_PAIR(TEXT_2_PLAYER));
		mvaddnstr(2, window.x - 6 - ft_strlen(tmp + 1),
				tmp, ft_strlen(tmp) - 1);
		mvaddstr(2, window.x - 6, " :2p");
		attroff(COLOR_PAIR(TEXT_2_PLAYER));
	}
	attroff(A_BOLD);
}

void			print_winner(char *line, t_cordinates window)
{
	int p1;
	int p2;

	p1 = ft_atoi(line + 9);
	getstr(line);
	p2 = (line) ? ft_atoi(line + 9) : 0;
	attron(A_BOLD);
	if (p1 > p2)
	{
		attron(COLOR_PAIR(TEXT_1_PLAYER));
		mvwaddstr(stdscr, 1, window.x / 2 - 9, "PLAYER 1 is WINNER");
	}
	else if (p1 < p2)
	{
		attron(COLOR_PAIR(TEXT_2_PLAYER));
		mvwaddstr(stdscr, 1, window.x / 2 - 9, "PLAYER 2 is WINNER");
	}
	else if (p1 == p2)
	{
		attron(COLOR_PAIR(TEXT_1_PLAYER));
		mvwaddstr(stdscr, 1, window.x / 2 - 8, "DRAW OR ");
		attron(COLOR_PAIR(TEXT_2_PLAYER));
		mvwaddstr(stdscr, 1, window.x / 2, "1 PLAYER");
	}
	attroff(A_BOLD);
}
