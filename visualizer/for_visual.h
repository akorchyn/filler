/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   for_visual.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 19:43:48 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/19 16:41:45 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_FOR_VISUAL_H
# define FILLER_FOR_VISUAL_H

# include <ncurses.h>
# include "../filler.h"

# define TEXT_1_PLAYER 1
# define TEXT_2_PLAYER 2
# define BACKGROUND_1 3
# define BACKGROUND_2 4
# define LAST_SET_1_PLAYER 5
# define LAST_SET_2_PLAYER 6
# define DEFAULT_WINDOW 7
# define BACKGROUND 8
# define BLACK_BACK 9
# define BLACK_BACK 9

void			print_winner(char *line, t_cordinates window);
void			print_names(char *line, t_cordinates window);
void			change_colors(t_matrix map, t_cordinates a, int x, int y);

#endif
