/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:04:11 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/19 17:00:12 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "for_visual.h"

static char		**read_map(int y)
{
	char	**map;
	char	*line;
	int		i;

	map = (char **)malloc(sizeof(char *) * (y + 1));
	i = -1;
	getstr(line);
	while (++i < y)
	{
		getstr(line);
		map[i] = ft_strdup(ft_strchr(line, ' ') + 1);
	}
	map[i] = NULL;
	return (map);
}

static void		get_x_y(int *x, int *y, char *line)
{
	int		i;

	i = -1;
	while (line[++i])
	{
		if (ft_isdigit(line[i]))
		{
			*y = ft_atoi(line + i);
			while (ft_isdigit(line[i]))
				i++;
			*x = ft_atoi(line + i);
			break ;
		}
	}
}

void			all_operations(t_matrix map, char *line,
		t_cordinates *window)
{
	t_cordinates a;

	while (ft_strncmp(line, "Plateau", 7))
		getstr(line);
	get_x_y(&map.x, &map.y, line);
	map.map = read_map(map.y);
	a.x = window->x / 2 - map.x / 2;
	a.y = window->y / 2 - map.y / 2;
	change_colors(map, a, -1, -1);
	refresh();
	ft_freesplit(map.map);
	while (ft_strncmp(line, "Plateau", 7))
	{
		if (!ft_strncmp(line, "== O", 4))
			return ;
		getstr(line);
	}
	all_operations(map, line, window);
}

void			init_colors(void)
{
	start_color();
	init_pair(TEXT_1_PLAYER, COLOR_BLUE, COLOR_BLACK);
	init_pair(TEXT_2_PLAYER, COLOR_RED, COLOR_BLACK);
	init_pair(LAST_SET_1_PLAYER, COLOR_CYAN, COLOR_CYAN);
	init_pair(LAST_SET_2_PLAYER, COLOR_MAGENTA, COLOR_MAGENTA);
	init_pair(BACKGROUND_2, COLOR_RED, COLOR_RED);
	init_pair(BACKGROUND_1, COLOR_BLUE, COLOR_BLUE);
	init_pair(DEFAULT_WINDOW, COLOR_YELLOW, COLOR_YELLOW);
	init_pair(BLACK_BACK, COLOR_BLACK, COLOR_BLACK);
	bkgd(COLOR_PAIR(BLACK_BACK));
}

int				main(void)
{
	char			*line;
	t_matrix		map;
	t_cordinates	window;
	t_cordinates	prev;

	map.map = NULL;
	initscr();
	curs_set(0);
	getmaxyx(stdscr, prev.y, prev.x);
	ft_printf("\e[3;0;0t\e[8;106;106t");
	window.y = 106;
	window.x = 106;
	noecho();
	init_colors();
	while (!getstr(line) && ft_strncmp(line, "Plateau", 7))
		if (!ft_strncmp("$$$ exec", line, 8))
			print_names(line, window);
	all_operations(map, line, &window);
	print_winner(line, window);
	refresh();
	sleep(3);
	clear();
	refresh();
	ft_printf("\e[8;%d;%dt", prev.y, prev.x);
	return (0);
}
