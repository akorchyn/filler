/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <akorchyn@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 15:46:05 by akorchyn          #+#    #+#             */
/*   Updated: 2018/12/20 14:34:50 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void			free_lists(t_cordinates *list)
{
	if (!list)
		return ;
	free_lists(list->next);
	free(list);
}

static void		get_x_y(int *x, int *y, char *line)
{
	int		i;

	i = -1;
	while (line[++i])
	{
		if (ft_isdigit(line[i]))
		{
			*y = ft_atoi(line + i);
			while (ft_isdigit(line[i]))
				i++;
			*x = ft_atoi(line + i);
			break ;
		}
	}
	free(line);
}

static char		**read_map(int y, int is_map)
{
	char	**map;
	char	*line;
	int		i;

	map = (char **)malloc(sizeof(char *) * (y + 1));
	i = -1;
	if (is_map)
	{
		get_next_line(0, &line);
		free(line);
	}
	while (++i < y)
	{
		get_next_line(0, &line);
		if (is_map)
			map[i] = ft_strdup(ft_strchr(line, ' ') + 1);
		else
			map[i] = ft_strdup(line);
		free(line);
	}
	map[i] = NULL;
	return (map);
}

static void		all_operations(t_matrix map, t_matrix figure, char *line)
{
	t_cordinates	*enemy_points;

	while (ft_strncmp(line, "Plateau", 7))
	{
		free(line);
		get_next_line(0, &line);
	}
	get_x_y(&map.x, &map.y, line);
	map.map = read_map(map.y, 1);
	get_next_line(0, &line);
	get_x_y(&figure.x, &figure.y, line);
	figure.map = read_map(figure.y, 0);
	enemy_points = fill_list(map);
	set(enemy_points, map, figure);
	ft_freesplit(map.map);
	ft_freesplit(figure.map);
	if (get_next_line(0, &line))
		all_operations(map, figure, line);
}

int				main(void)
{
	t_matrix	map;
	t_matrix	figure;
	char		*line;

	map.x = -1;
	figure.x = -1;
	get_next_line(0, &line);
	if (ft_strstr(line, "p2"))
		g_player = 'x';
	else
		g_player = 'o';
	g_enemy = (g_player == 'o') ? 'x' : 'o';
	all_operations(map, figure, line);
	return (0);
}
