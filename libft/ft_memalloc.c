/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmalloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 15:07:10 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:18:28 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*arr;
	int		i;

	i = -1;
	if (!size)
		return (NULL);
	arr = (char *)malloc(size);
	if (!arr)
		return (NULL);
	while (++i < (int)size)
		arr[i] = '\0';
	return (arr);
}
