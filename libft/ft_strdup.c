/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 14:20:52 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:15:23 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *src)
{
	char	*ans;
	int		i;

	i = 0;
	ans = (char *)malloc(ft_strlen(src) + 1);
	if (!ans)
		return (NULL);
	while (src[i] != '\0')
	{
		ans[i] = ((char *)src)[i];
		i++;
	}
	ans[i] = '\0';
	return (ans);
}
